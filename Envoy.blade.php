@servers(['web' => 'deployer@192.99.127.219'])

@setup
    $repository = 'git@gitlab.com:mehranrasulian/laravel-sample.git';
    $releases_dir = '/var/www/releases';
    $app_dir = '/var/www/app';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy')
    clone_repository
    run_composer
    update_symlinks
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }};
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }};
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})";
    cd {{ $new_release_dir }};
    composer install --prefer-dist --no-scripts -q -o;
@endtask

@task('update_symlinks')
    echo 'Linking current release';
    ln -nfs {{ $new_release_dir }} {{ $app_dir }};
@endtask